FROM postgres:9.6

ENV PG_MAJOR 9.6

ENV POSTGRES_USER postgres
ENV POSTGRES_PASSWORD password

WORKDIR /

COPY docker-healthcheck /usr/local/bin/

COPY ./init-user.sh /docker-entrypoint-initdb.d
COPY sql /docker-entrypoint-initdb.d/sql
COPY data /docker-entrypoint-initdb.d/data
