#!/usr/bin/env bash

# create the db

psql -v ON_ERROR_STOP=1 --username "postgres" <<-EOSQL
    CREATE DATABASE "test" OWNER postgres;
    GRANT ALL PRIVILEGES ON DATABASE "test" to postgres;
EOSQL

# create the schema

psql -v ON_ERROR_STOP=1 --username "postgres" -d test -f /docker-entrypoint-initdb.d/sql/zoo_create_schema.sql

# load the data

psql -v ON_ERROR_STOP=1 --username "postgres" -d test <<-EOSQL
    \COPY zs."FeedingFrequency" ("animal","frequency") FROM '/docker-entrypoint-initdb.d/data/feeding_frequency.dat';
    \COPY zs."AnimalWeight" ("animal", "weight", "idealweight", "name") FROM '/docker-entrypoint-initdb.d/data/animal_weight.dat';
    \COPY zs."Meal" ("ishealthy", "mealdate", "vegetable", "fruit", "animalname", "animal") FROM '/docker-entrypoint-initdb.d/data/meal.dat';
EOSQL
