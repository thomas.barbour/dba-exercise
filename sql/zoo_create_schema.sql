DROP SCHEMA IF EXISTS zs CASCADE;

Create SCHEMA zs;

CREATE TABLE IF NOT EXISTS zs."AnimalWeight" (
    AnimalWeightId BIGSERIAL PRIMARY KEY,
    Name char(128),
    Animal char(64),
    Weight Real,
    IdealWeight Real
);

CREATE TABLE IF NOT EXISTS zs."Meal" (
    MealId BIGSERIAL PRIMARY KEY,
    IsHealthy char(10),
    MealDate int,
    Vegetable char(128),
    Fruit char(128),
    AnimalName char(64),
    Animal char(128)
);

CREATE INDEX ON zs."Meal" (
  upper(AnimalName)
);

CREATE TABLE IF NOT EXISTS zs."FeedingFrequency" (
    FeedingId BIGSERIAL PRIMARY KEY,
    Animal char(128),
    Frequency BIGINT
);
