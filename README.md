# DBA Exercise

## REQUIREMENTS

  The candidate should have a fairly recent version of postgres to do this test.
Specifically, this version was written against postgres 9.6.  Additionally, the
candidate can modify the init_user.sh script to his/her liking to adjust for
different connection strings or a non-bash environment.
To populate the database simply run the ./init_user.sh file OR if you have docker installed:

`docker-compose build --up -d`

OR if you don't have compose installed

`docker build -t dba:test .;docker run -d -p 5432:5432 dba:test`

and the service will be available on localhost:5432 from your favorite Database Management Software

## BACKGROUND

  A zoo staff wants to more accurately track their animals eating habits and hence
their overall health. In order to accomplish this, the staff created three
tables concerning: Meal time's for all of the animals in the zoo, a table of the
individual animal type and how frequently they eat, and finally, the
weight/ideal weight for each animal in the zoo.

The zoo staff loves their animals, and named each one of them -- however, some
names between the animal types may not be unique. (eg. Audrey is not only a
reticulated giraffe, but also a simple domestic goat.)

Please refer to the zoo_erd.pdf for the *INTENDED* schema relationships.

## PROBLEM

The zoo staff has realized that they have a limited understanding of databases
and have noticed long query times for returning simple reports.
They wish to improve the query times as well as reduce data integrity issues.

1. Report Query
  * Retrieve a full zoo report consisting of the following fields when the animal is healthy:
    - animal name, animal type, ishealthy, weight, idealweight, frequency, mealdate, vegetable, and fruit
  * Include rows that have potential data-integrity issues. Every result row should not duplicate another row. Include your query with the result.
    - Get the total row count
    - Get the database's result for a detailed query execution time.

2. Schema Improvements
    * Could this schema be improved to reduce the query time?
    * If so, what would be some example improvements and what methodology would you use?
    * Do you have any concerns with these query speed improvements?

3. Tuning improvements
    * Could global database settings be changed to improve the query time?
    * If so, what would you change and why?

4. Error Handling
    * Seeing the intention of the zoo_erd, could you make any improvements in the schema to reduce errors?